// imports
const Discord = require("discord.js");
require("dotenv").config();
const ytdl = require("ytdl-core");

// instances
const bot = new Discord.Client();
// let usrs = Object.values(bot.channels);

// constants
const configs = {
  token: process.env.TOKEN,
  prefix: "gallo",
  available: [
    "crow-me", // join to voice channel and crows
    "play", // play music from link
    "queue", // shows queue if exist a queue
    "next",
    "disconnect",
    "cm", // variant to crow-me
    "p", // variant to play
    "q", // variant to queue
    "n", // variant to next
    "d", // variant to disconnect
  ],
};

// bot code
bot.on("ready", () => {
  // console.log(`Bot obj ${JSON.stringify(bot)}`);
  // console.log(typeof usrs);
});

bot.on("message", (message) => {
  if (message.author.bot) return;
  // gallo play https://www.link.com
  // if (message.content.startsWith(`${prefix}play`)) {
  //   execute(message, serverQueue);
  //   return;
  // }
});

const action = (channel, option) => {
  bot.joinVoiceChannel(channel, callback);
};

bot.login(configs.token);
